import React, {Component} from 'react';
import axios from 'axios';

class Contact extends Component {

    state = {
        Contact: {}
    };

    componentDidMount() {
        this.getContact();
    }

    getContact() {
        this.setState({loading: true});

        axios.get('/Contact.json').then(response => {
            this.setState({Contact: response.data});
            console.log(this.state.Contact);
        })
    };

    render() {

        return (
            <div className="Container">
                <span className="Name">{this.state.Contact.Name}</span>
                <span className="address">{this.state.Contact.address}</span>
                <span className="Mail">{this.state.Contact.email}</span>
                <span className="Phone">{this.state.Contact.phone}</span>
                <span className="Shedule">{this.state.Contact.schedule}</span>
            </div>
        )
    }
};

export default Contact;