import React, {Component} from 'react';
import './Loader.css';

class Loader extends Component {

    render() {
        return (
            <div className="Holder">
                <div className="Preloader">
                    <div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>
                </div>
            </div>
        );
    };
}

export default Loader;

