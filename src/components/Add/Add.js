import React, {Component} from 'react';
import './Add.css';
import axios from 'axios';
import moment from 'moment'


class Add extends Component {

    state = {
        post: {
            title: '',
            body: '',
            date:''
        },
        loading: false
    };

    componentDidMount() {
        console.log(this.props.match.path);
    if (this.props.match.path.includes("edit")){

        console.log('редактируем');
    }
        console.log(this.props, '[NoteAdd] did Mount')

    }

    componentDidUpdate() {
        console.log(this.props, '[NoteAdd] did Update')
    }

    itemCreate = event => {
        event.preventDefault();
        this.setState({loading: true});

        let post = {
            title: this.state.post.title,
            body: this.state.post.body,
            date: moment()
        };
        axios.post('/items.json', post).then(() => {
            this.setState({loading: false});
            this.props.history.replace('/');
        });

        this.setState({post});
    };

    valueChanged = event => {
        event.persist();

        this.setState(prevState => {
            console.log(this.state.post);
            return {
                post: {...prevState.post, [event.target.name]: event.target.value}
            };
        });
    };

    render() {

        return (

            <form className="NoteAdd Container">
                <input type="text" name="title" placeholder="New note title" value={this.state.post.title}
                       onChange={this.valueChanged}/>
                <textarea name="body" placeholder="Enter your notes..." value={this.state.post.body}
                          onChange={this.valueChanged}/>
                <button onClick={this.itemCreate}>Add</button>
            </form>
        )
    }


}

export default Add;