import React from 'react';
import './NotFound.css'

const NotFound = props => {
    return (
        <div className="NotFound Container">
            <h1>Error 404: not found</h1>
        </div>
)
};

export default NotFound;