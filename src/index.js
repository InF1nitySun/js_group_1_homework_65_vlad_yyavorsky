import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './container/App/App';
import registerServiceWorker from './registerServiceWorker';
import BrowserRouter from "react-router-dom/es/BrowserRouter";
import axios from 'axios';

axios.defaults.baseURL = 'https://homework65-2b68c.firebaseio.com/';

ReactDOM.render(<BrowserRouter><App /></BrowserRouter>, document.getElementById('root'));
registerServiceWorker();
