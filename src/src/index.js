import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/App/App'
import registerServiceWorker from './registerServiceWorker';
import BrowserRouter from "react-router-dom/es/BrowserRouter";
import axios from 'axios';

axios.defaults.baseURL = 'https://lab64-9a996.firebaseio.com/';

ReactDOM.render(<BrowserRouter><App /></BrowserRouter>, document.getElementById('root'));
registerServiceWorker();
