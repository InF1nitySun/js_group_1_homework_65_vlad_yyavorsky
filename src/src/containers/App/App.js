import React, {Component, Fragment} from 'react';
import {Route, Switch} from 'react-router-dom';
import './App.css';
import HomePage from "../../../container/HomePage/HomePage";
import Add from "../../../components/Add/Add";
import Item from "../../components/Notes/Item/Item";
import NotFound from "../../components/Page/Error/NotFound/NotFound";
import About from "../../../components/About/About";
import Contact from "../../../components/Contact/Contact";
import Navigation from "../../components/Page/Header/Navigation/Navigation";


class App extends Component {

    render() {
        return (
            <Fragment>
                <Navigation/>
                <Switch>
                    <Route path="/" exact component={HomePage}/>
                    <Route path="/posts" component={HomePage}/>
                    <Route path="/add"  component={Add}/>
                    <Route path="/posts/add"  component={Add}/>
                    <Route path="/about"  component={About}/>
                    <Route path="/contact"  component={Contact}/>
                    <Route path="/items/:id/edit" component={Add} />
                    <Route path="/items/:id"  component={Item}/>

                    <Route render={() => <NotFound style={{textAlign: 'center'}} component={NotFound}/>}/>
                </Switch>
            </Fragment>
        );
    }
}

export default App;
