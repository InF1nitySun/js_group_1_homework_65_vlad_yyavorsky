import React, {Component} from 'react';
import './Items.css';
import Moment from 'react-moment';
import {Link} from "react-router-dom";


class Items extends Component {



    render() {
        console.log(this.props);
        return (
            <div>
                <ul className='Items'>
                    {Object.keys(this.props.posts).map(id => {
                        return <li className='Item' key={id}>
                            <Moment format="YYYY.MM.DD HH:mm">{this.props.posts[id].date}</Moment>
                            <h3>{this.props.posts[id].title}</h3>
                            <Link  to={`/items/${id}`} >Read more >></Link>
                            <button className="But Edit">Edit</button>
                            <button className="But Delete" onClick={()=>this.props.delItem(id)}>Delete</button>
                        </li>
                    })}
                </ul>
            </div>
        )
    }
}

export default Items;