import React, {Component} from 'react';
import axios from 'axios';
import Moment from 'react-moment';
import Spinner from "../../../../components/Spinner/Spinner";
import './Item.css';

class Item extends Component {

    state = {
        loadedPost: null,
    };


    componentDidMount() {
        const id = this.props.match.params.id;

        axios.get(`/items/${id}.json`).then((response) => {

            this.setState({loadedPost: response.data})
        })
    }


    render() {
        console.log(this.props);
        if (this.state.loadedPost) {
            return (
                <div className="Item Container">
                    <h3 className="Title">{this.state.loadedPost.title}</h3>
                    <Moment format="DD.MM.YYYY HH:mm" className="Date">{this.state.loadedPost.date}</Moment>
                    <span className="Body">{this.state.loadedPost.body}</span>


                </div>
            )
        }
        else {
            return <Spinner/>;
        }
    }
}

export default Item;