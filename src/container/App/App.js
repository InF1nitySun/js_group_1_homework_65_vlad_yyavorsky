import React, {Component, Fragment} from 'react';
import './App.css';
import Loader from "../../components/Loader/Loader";
import Navigation from "../../components/Page/Header/Navigation/Navigation";
import {Route, Switch} from "react-router-dom";
import NotFound from "../../components/Page/Error/NotFound/NotFound";
import Test from "../../components/Test/Test";

class App extends Component {

    state = {
        test: 'test test test test test test '
    };


    render() {
        console.log(this.state.test);
        return (
            <Fragment>
                <Navigation/>
                <Loader/>
                <Switch>
                    <Route path="/test" exact component={Test}/>
                    {/*<Route path="/item/:id"  component={ItemInfo}/>*/}
                    <Route render={() => <NotFound style={{textAlign: 'center'}} component={NotFound}/>}/>
                </Switch>
            </Fragment>
        );
    }
}

export default App;
