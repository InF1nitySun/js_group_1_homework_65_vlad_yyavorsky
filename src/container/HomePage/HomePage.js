import React, {Component} from 'react';
import './HomePage.css';
import axios from 'axios';
import Spinner from "../../../components/Spinner/Spinner";
import Items from "../../components/Notes/Items/Items";

class HomePage extends Component {

    state = {
        posts: [],
        loading: false
    };

    componentDidMount() {
        this.getPosts();
    }

    getPosts = event => {
        this.setState({loading: true});

        axios.get('/items.json').then(response => {
            response.data ?
            this.setState({posts: response.data}) : this.setState({posts: []});
            this.props.history.replace('/');
            this.setState({loading: false});
        });
    };

    delItem = id => {
        axios.delete(`items/${id}.json`).then(() => {
            this.getPosts();
        })
    };

    render() {

        let form = (
            <Items posts={this.state.posts} delItem={this.delItem}/>
        );
        if (this.state.loading) {
            form = <Spinner/>;
        }
        return (
            <div className="HomePage Container">

                <div>
                    <h2>Лабораторная работа 64</h2>
                    {form}
                </div>
            </div>
        )
    }
};

export default HomePage;